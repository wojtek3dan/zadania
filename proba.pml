[doc title = Zbiór zadań z optymalizacji dyskretnej \
    authors = Wojciech Wieczorek \
    date = 2021-07-18

    [ch title = Drzewa sufiksowe
        [list html_style="list-style-type:decimal"
            [el
                [p Znajdź w podanym łańcuchu liczbę znaków najdłuższego prefiksu będącego 
palindromem. Na przykład dla wejściowego łańcucha [c babcbabbca] program powinien dać 
odpowiedź równą 7, bo najdłuższym palindromem rozpoczynającym podany łańcuch jest [c babcbab].
Zakładamy, że wejściowy łańcuch składa się z liter [i a]–[i z], [i A]–[i Z], a jego długość 
może wynosić 1 × 10[sup 6]. Limit czasowy \= 10 sekund.]

                [input title = Przykładowe dane wejściowe
                    aaaabb
                input]

                [output title = Spodziewany wynik
                    4
                output]

                [admon label = Uwagi
                    Można skonstruować efektywny algorytm, korzystając z drzewa sufiksowego.
                ]
            ]
            [el
                [p Dla danego tekstu wskaż jego najdłuższy podłańcuch, który występuje w tym 
tekście więcej niż raz. Na przykład, jeśli tekstem jest [i t]\[0..7\] \= [i bananami], to
szukanym podłańcuchem jest [i t]\[3..5\] ([i t]\[1..3\] też jest poprawną odpowiedzią). Zakładamy, 
że wejściowy łańcuch składa się z liter [i a]–[i z], [i A]–[i Z], a jego długość może wynosić 
1 × 10[sup 6]. Limit czasowy \= 10 sekund.]

                [input title = Przykładowe dane wejściowe
                    aaaabb
                input]

                [output title = Spodziewany wynik
                    0..2
                output]

                [admon label = Uwagi
                    Można skonstruować efektywny algorytm, korzystając z drzewa sufiksowego.
                ]
            ]
        ]
    ]

    [ch title = Treaps
        [list html_style="list-style-type:decimal"
            [el
                [p Dana jest [i n]-elementowa tablica [i a] liczb całkowitych 32-bitowych.
Naszym celem jest określenie dla każdego indeksu [i i], ile liczb znajdujących się pod 
indeksem większym niż [i i] jest mniejszych od [i a]\[[i i]\]. Na wyjściu oczekujemy odpowiedniej 
[i n]-elementowej tablicy. Zakładamy, że 1 ≤ [i n] ≤ 10[sup 6] oraz że dane wejściowe 
i wyjściowe zapisywane są w plikach JSON. Limit czasowy \= 10 sekund.]

                [input title = Przykładowe dane wejściowe
                    [5, 4, 7, 9, -2, 4, 4, 5, 6]
                input]

                [output title = Spodziewany wynik
                    [4, 1, 5, 5, 0, 0, 0, 0, 0]
                output]

                [admon label = Uwagi
                    Można skonstruować efektywny algorytm, korzystając z drzewa treap.
                ]
            ]
        ]
    ]
]